﻿using UnityEngine;
using UnityEngine.Events;

public class Cell : MonoBehaviour, ISetCell
{
    // Serializables

    /// <summary>
    /// Column where the Cell is located
    /// </summary>
    [SerializeField]
    private int m_iColumn;

    /// <summary>
    /// Line where the Cell is located
    /// </summary>
    [SerializeField]
    private int m_iLine;

    /// <summary>
    /// This stores the drag event of our cell
    /// </summary>
    [SerializeField]
    private InputEvent m_pDragEvent;

    /// <summary>
    /// This stores the release event of our cell
    /// </summary>
    [SerializeField]
    private InputEvent m_pReleaseEvent;

    /// <summary>
    /// This class defines how the InputEvent is supposed to work. It returns the ICell where the input occurred and
    /// a mouse position, which indicates where the user clicked to create the input event.
    /// </summary>
    [System.Serializable]
    private class InputEvent : UnityEvent<ICell, Vector3> { }

    // Non-Serializables

    /// <summary>
    /// Stores the IGem of our Cell
    /// </summary>
    private IGem m_pGem;

    /// <summary>
    /// This flag returns true when the user starts dragging the mouse over the cell
    /// </summary>
    private bool m_bStartDrag;

    #region MonoBehaviour

    protected virtual void OnEnable()
    {
        m_bStartDrag = false;
    }

    protected virtual void OnDisable()
    {
        if (m_pGem != null)
        {
            // If our gem has an IPoolableObject, we try to recycle it instead of destroying it, to ensure reusability.
            IPoolableObject pPoolableObject = m_pGem.GameObject.GetComponent<IPoolableObject>();
            if (pPoolableObject != null)
                pPoolableObject.Recycle();
            else
                Destroy(m_pGem.GameObject); // otherwise, it is destroyed
        }
    }

    /// <summary>
    /// This method is invoked when the user stars dragging the mouse over our object. 
    /// It keeps being called as long as the player keeps dragging. Even if the mouse is not over the object anymore.
    /// </summary>
    protected virtual void OnMouseDrag()
    {
        if (m_pDragEvent != null)
        {
            m_bStartDrag = true; // since the drag started, we set this to true
            m_pDragEvent.Invoke(this, Input.mousePosition);
        }
    }

    /// <summary>
    /// Method invoked when the user releases the mouse, regardless if it was over the cell or not.
    /// </summary>
    protected virtual void OnMouseUp()
    {
        // we only execute the release event if, before that, the player was dragging the mouse over this cell
        if (m_pReleaseEvent != null && m_bStartDrag)  
        {
            m_pReleaseEvent.Invoke(this, Input.mousePosition);
            m_bStartDrag = false; // since the drag stopped, we set this to false
        }
    }

    #endregion

    #region ISetCell

    /// <summary>
    /// Column where the ICell is located
    /// </summary>
    public int Column
    {
        get
        {
            return m_iColumn;
        }
    }

    /// <summary>
    /// Line where the ICell is located
    /// </summary>
    public int Line
    {
        get
        {
            return m_iLine;
        }
    }

    /// <summary>
    /// IGem reference from our ICell, in case it has one
    /// </summary>
    public IGem Gem
    {
        get
        {
            return m_pGem;
        }
    }

    /// <summary>
    /// World position of our ICell
    /// </summary>
    public Vector3 Position
    {
        get
        {
            return transform.position;
        }
    }

    /// <summary>
    /// GameObject reference from our ICell
    /// </summary>
    public GameObject GameObject
    {
        get
        {
            return gameObject;
        }
    }

    /// <summary>
    /// Initialization setup. Defines a column, a line and a global world position for the ICell. You can also insert a IGem right away.
    /// </summary>
    /// <param name="iColumn">Column of the cell</param>
    /// <param name="iLine">Line of the cell</param>
    /// <param name="vPosition">World position of the cell</param>
    /// <param name="pGem">Gem of the cell</param>
    public void SetupCell(int iColumn, int iLine, Vector3 vPosition, IGem pGem = null)
    {
        m_iColumn = iColumn;
        m_iLine = iLine;
        ChangeGem(pGem);
        transform.position = vPosition;
    }

    /// <summary>
    /// Changes the current gem being hold by the ICell. If bDestroyCurrent is true, the current gem is recycled or destroyed before it is changed by pGem.
    /// </summary>
    /// <param name="pGem">Gem that is going to substitute the current gem</param>
    /// <param name="bDestroyCurrent">If true the current gem will be either destroyed or recycled</param>
    public void ChangeGem(IGem pGem, bool bDestroyCurrent = true)
    {
        // As stated before, if bDestroyCurrent is true we try to either recycle or destroy the gem
        if (m_pGem != null && bDestroyCurrent)
        {
            // Recycling will only occur if current gem has an IPoolableObjects
            IPoolableObject pPoolableObject = m_pGem.GameObject.GetComponent<IPoolableObject>();
            if (pPoolableObject != null)
                pPoolableObject.Recycle();
            else
                Destroy(m_pGem.GameObject); // otherwise, it is destroyed
        }

        // if the new gem is different than null, not only we insert it into the Cell,
        // but also adjust position, scale, rotation and parenting to respect the boundaries of this Cell
        if (pGem != null) 
        {
            m_pGem = pGem; 
            Transform pGemTransform = pGem.GameObject.transform;
            pGemTransform.position = transform.position;
            pGemTransform.localScale = transform.lossyScale;
            pGemTransform.rotation = transform.rotation;
            pGemTransform.parent = transform;
        }
    }

    /// <summary>
    /// Adds a Drag Listener to the ICell. The 'pCall' method will be called every time a drag gesture occurs over ICell. It needs a trigger collider, by the way.
    /// </summary>
    /// <param name="pCall">UnityAction method that is called when a drag event occurs</param>
    public void AddDragListener(UnityAction<ICell, Vector3> pCall)
    {
        if (m_pDragEvent != null)
            m_pDragEvent.AddListener(pCall);
    }

    /// <summary>
    /// Removes a Drag Listener from the ICell. After that, 'pCall' will not be called after a drag gesture anymore.
    /// </summary>
    /// <param name="pCall">UnityAction method that stops being called when a drag event occurs</param>
    public void RemoveDragListener(UnityAction<ICell, Vector3> pCall)
    {
        if (m_pDragEvent != null)
            m_pDragEvent.RemoveListener(pCall);
    }

    /// <summary>
    /// Adds a Release Listener to the ICell.  The 'pCall' method will be called every time a release gesture occurs over ICell. It needs a trigger collider, by the way.
    /// </summary>
    /// <param name="pCall">UnityAction method that is called when a release event occurs</param>
    public void AddReleaseListener(UnityAction<ICell, Vector3> pCall)
    {
        if (m_pReleaseEvent != null)
            m_pReleaseEvent.AddListener(pCall);
    }

    /// <summary>
    /// Removes a Release Listener to the ICell. After that, 'pCall' will not be called after a release gesture anymore.
    /// </summary>
    /// <param name="pCall">UnityAction method that stops being called when a release event occurs</param>
    public void RemoveReleaseListener(UnityAction<ICell, Vector3> pCall)
    {
        if (m_pReleaseEvent != null)
            m_pReleaseEvent.RemoveListener(pCall);
    }

    #endregion
}
