﻿using UnityEngine;

public class StringGemType : MonoBehaviour, IGemType
{
    // Serializables

    /// <summary>
    /// Sprite Renderer used by this class to help define his own type
    /// </summary>
    [SerializeField]
    private SpriteRenderer m_pSpriteRenderer;

    // Non-Serializables

    /// <summary>
    /// String description of our Gem Type. 
    /// The description will come from the name of the sprite assigned to the Sprite Renderer.
    /// </summary>
    private string m_sGemDescription;

    #region MonoBehaviour

    protected virtual void OnValidate()
    {
        // Validating SpriteRenderer. It cannot be null and must have a sprite assigned to it.
        if (m_pSpriteRenderer == null)
        {
            m_pSpriteRenderer = GetComponent<SpriteRenderer>();

            if (m_pSpriteRenderer == null)
            {
                Debug.Log("[StringGemType]: m_pSpriteRenderer cannot be null");
                return;
            }
        }

        if (m_pSpriteRenderer.sprite == null)
        {
            Debug.Log("[StringGemType]: m_pSpriteRenderer.sprite cannot be null");
            return;
        }
        else
        {
            m_sGemDescription = m_pSpriteRenderer.sprite.name;
        }
    }

    protected virtual void OnEnable()
    {
        m_pSpriteRenderer = GetComponent<SpriteRenderer>();

        // this is what is going to differentiate a type from another. 
        // If two Gem Types have a Sprite Renderer with equally named Sprites, they are supposed to be equal.
        m_sGemDescription = m_pSpriteRenderer.sprite.name; 
    }

    #endregion

    #region IGemType

    /// <summary>
    /// Returns true if both types are the same
    /// </summary>
    /// <param name="pGemType">IGemType that is going to be compared</param>
    /// <returns>Returns true if both types are the same</returns>
    public bool Equals(IGemType pGemType)
    {
        return GetHashCode() == pGemType.GetHashCode();
    }

    /// <summary>
    /// Returns the hash code of our type. 
    /// The prime numbers are there to help differentiate this object from the m_sGemDescription hash code.
    /// </summary>
    /// <returns>A hash value that is supposed to be unique to this type</returns>
    public override int GetHashCode()
    {
        int iHash = 17;
        iHash = iHash * 23 * m_sGemDescription.GetHashCode();
        return iHash;
    }

    #endregion
}
