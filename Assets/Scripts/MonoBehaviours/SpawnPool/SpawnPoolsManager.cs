﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnPoolsManager : MonoBehaviour, ISpawnPoolsManager
{
    // Serializables
    [SerializeField]
    private GameObject[] m_lSpawnPoolsComponents;
    [SerializeField]
    private int m_iDefaultInitialSize = 10;

    // Non-Serializables

    /// <summary>
    /// List of SpawnPools that are part of this SpawnPoolsManager
    /// </summary>
    private List<ISpawnPool> m_lSpawnPools;

    /// <summary>
    /// Flag that indicates that SpawnPoolsManager is already initialized
    /// </summary>
    private bool m_bInitialized;

    #region MonoBehaviours

    protected virtual void OnValidate()
    {
        // Validating m_lSpawnPools
        if (m_lSpawnPoolsComponents == null)
        {
            Debug.LogError("[SpawnPoolsManager]: m_lSpawnPoolsComponents cannot be null");
            return;
        }

        if (m_lSpawnPoolsComponents.Length <= 0)
        {
            Debug.LogError("[SpawnPoolsManager]: m_lSpawnPoolsComponents cannot be smaller or equal to zero");
            return;
        }

        for (int i = 0; i < m_lSpawnPoolsComponents.Length; i++)
        {
            if (m_lSpawnPoolsComponents[i] == null)
            {
                Debug.LogError("[SpawnPoolsManager]: m_lSpawnPoolsComponents cannot have null elements");
                return;
            }

            ISpawnPool pSpawnPool = m_lSpawnPoolsComponents[i].GetComponent<ISpawnPool>();
            if (pSpawnPool == null)
            {
                Debug.LogError("[SpawnPoolsManager]: every element in m_lSpawnPoolsComponents needs to have an ISpawnPool component");
                return;
            }
        }
    }

    protected virtual void OnEnable()
    {
        Initialize();
    }

    #endregion

    #region SpawnPoolsManager

    /// <summary>
    /// Initializes SpawnPoolsManager
    /// </summary>
    private void Initialize()
    {
        if (m_lSpawnPools != null)
            m_lSpawnPools.Clear();

        string sName = "[SpawnPoolsManager]";
        gameObject.name = sName;

        // On initialization, we create a pool for each SpawnPool available
        m_lSpawnPools = new List<ISpawnPool>();
        for (int i = 0; i < m_lSpawnPoolsComponents.Length; i++)
        {
            ISpawnPool pSpawnPool = m_lSpawnPoolsComponents[i].GetComponent<ISpawnPool>();
            pSpawnPool.CreatePool();
            m_lSpawnPools.Add(pSpawnPool);
        }

        m_bInitialized = true;
    }

    #endregion

    #region ISpawnPoolsManager

    /// <summary>
    /// Recycles a spawned copy from one of our Spawn Pools. If there's no Spawn Pool for said prefab, the instance is destroyed.
    /// </summary>
    /// <param name="pSpawn">Copy that is going to be recycled</param>
    public void Recycle(GameObject pSpawn)
    {
        if (!m_bInitialized)
            Initialize();

        if (m_bInitialized && m_lSpawnPoolsComponents != null)
        {
            for (int i = 0; i < m_lSpawnPools.Count; i++)
            {
                ISpawnPool pSpawnPool = m_lSpawnPools[i];
                if (pSpawnPool.Instances != null && pSpawnPool.Instances.Contains(pSpawn))
                {
                    pSpawnPool.Recycle(pSpawn);
                    return;
                }
            }
        }
    }

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <returns>The spawned object</returns>
    public GameObject Spawn(GameObject pPrefab)
    {
        return Spawn(pPrefab, Vector3.zero, null, Quaternion.identity, Vector3.one);
    }

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <returns>The spawned object</returns>
    public GameObject Spawn(GameObject pPrefab, Vector3 vPosition)
    {
        return Spawn(pPrefab, vPosition, null, Quaternion.identity, Vector3.one);
    }

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <param name="pParent">Parent of the spawned object</param>
    /// <returns>The spawned object</returns>
    public GameObject Spawn(GameObject pPrefab, Vector3 vPosition, Transform pParent)
    {
        return Spawn(pPrefab, vPosition, pParent, Quaternion.identity, Vector3.one);
    }

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <param name="pParent">Parent of the spawned object</param>
    /// <param name="qRotation">Initial rotation of the spawned object</param>
    /// <returns>The spawned object</returns>
    public GameObject Spawn(GameObject pPrefab, Vector3 vPosition, Transform pParent, Quaternion qRotation)
    {
        return Spawn(pPrefab, vPosition, pParent, qRotation, Vector3.one);
    }

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <param name="pParent">Parent of the spawned object</param>
    /// <param name="qRotation">Initial rotation of the spawned object</param>
    /// /// <param name="vScale">Initial scale of the spawned object.</param>
    /// <returns>The spawned object</returns>
    public GameObject Spawn(GameObject pPrefab, Vector3 vPosition, Transform pParent, Quaternion qRotation, Vector3 vScale)
    {
        if (!m_bInitialized)
            Initialize();

        if (m_bInitialized && m_lSpawnPoolsComponents != null)
        {
            for (int i = 0; i < m_lSpawnPools.Count; i++)
            {
                ISpawnPool pSpawnPool = m_lSpawnPools[i];
                if (pSpawnPool.Prefab != null && pSpawnPool.Prefab.Equals(pPrefab))
                {
                    return pSpawnPool.Spawn(vPosition, pParent, qRotation, vScale);
                }
            }

            // if the pPrefab is not part of any SpawnPool, we create a new one.
            GameObject pNewSpawnPoolGameObject = new GameObject();
            pNewSpawnPoolGameObject.transform.parent = transform;
            ISpawnPool pNewSpawnPool = pNewSpawnPoolGameObject.AddComponent<SpawnPool>();
            pNewSpawnPool.CreatePool(m_iDefaultInitialSize, pPrefab);
            m_lSpawnPools.Add(pNewSpawnPool);
            return pNewSpawnPool.Spawn(vPosition, pParent, qRotation, vScale);

        }

        return null;
    }

    #endregion
}
