﻿using UnityEngine;

public class PoolableObject : MonoBehaviour, IPoolableObject
{
    #region IPoolableObject

    private ISpawnPool m_pSpawnPool;

    /// <summary>
    /// Initializes the Spawned Object.
    /// </summary>
    /// <param name="pSpawnPool">Spawn Pool that spawns this object</param>
    /// <returns>True if the IPoolableObject was initialized</returns>
    public bool Initialize(ISpawnPool pSpawnPool)
    {
        m_pSpawnPool = pSpawnPool;
        return m_pSpawnPool != null;
    }

    public void Recycle()
    {
        if (m_pSpawnPool != null)
            m_pSpawnPool.Recycle(gameObject);
    }

    public GameObject Spawn()
    {
        if (m_pSpawnPool != null)
            return m_pSpawnPool.Spawn();
        return null;
    }

    public GameObject Spawn(Vector3 vPosition)
    {
        if (m_pSpawnPool != null)
            return m_pSpawnPool.Spawn(vPosition);
        return null;
    }

    public GameObject Spawn(Vector3 vPosition, Transform pParent)
    {
        if (m_pSpawnPool != null)
            return m_pSpawnPool.Spawn(vPosition, pParent);
        return null;
    }

    public GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation)
    {
        if (m_pSpawnPool != null)
            return m_pSpawnPool.Spawn(vPosition, pParent, qRotation);
        return null;
    }

    public GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation, Vector3 vScale)
    {
        if (m_pSpawnPool != null)
            return m_pSpawnPool.Spawn(vPosition, pParent, qRotation, vScale);
        return null;
    }

    #endregion
}
