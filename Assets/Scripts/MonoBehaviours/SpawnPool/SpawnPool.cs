﻿using System.Collections.Generic;
using UnityEngine;

public class SpawnPool : MonoBehaviour, ISpawnPool
{
    // Non-Serializables
    [SerializeField]
    private GameObject m_pPrefab;
    [SerializeField]
    private int m_iInitialSize = 10;

    // Serializables
    private List<GameObject> m_lAllInstances;

    #region MonoBehaviours

    protected virtual void OnValidate()
    {
        if (m_iInitialSize <= 0)
        {
            m_iInitialSize = 1;
            Debug.LogWarning("[SpawnPool]: m_iInitialSize cannot be smaller than or equal to zero");
        }
    }

    protected virtual void OnDisable()
    {
        DestroyPool();
    }

    #endregion

    #region SpawnPool

    /// <summary>
    /// A readable reference to the Prefab used by this ISpawnPool to create another copies
    /// </summary>
    public GameObject Prefab
    {
        get
        {
            return m_pPrefab;
        }
    }

    /// <summary>
    /// List of prefab copies from our pool, regardless if they are spawned in the scene or not.
    /// </summary>
    public IList<GameObject> Instances
    {
        get
        {
            return m_lAllInstances;
        }
    }

    /// <summary>
    /// Creates a pool with a pPrefab with iInitialSize GameObjects instantiated on pool creation
    /// </summary>
    /// <param name="iInitialSize">initial size of pool</param>
    /// <param name="pPrefab">prefab of our pool</param>
    public void CreatePool(int iInitialSize = 0, GameObject pPrefab = null)
    {
        if (iInitialSize > 0)
            m_iInitialSize = iInitialSize;

        if (pPrefab != null)
            m_pPrefab = pPrefab;

        try
        {
            if (m_pPrefab == null)
                throw new System.ArgumentNullException("m_pPrefab");

            IPoolableObject pPoolableObject = m_pPrefab.GetComponent<IPoolableObject>();
            if (pPoolableObject != null)
                pPoolableObject.Initialize(this);

            string sPoolName = string.Concat(m_pPrefab.name, " [SpawnPool]");
            gameObject.name = sPoolName;

            m_lAllInstances = new List<GameObject>(m_iInitialSize);
            for (int i = 0; i < m_iInitialSize; i++)
            {
                GameObject pInstance = Instantiate(m_pPrefab, m_pPrefab.transform.position, m_pPrefab.transform.rotation);
                pInstance.SetActive(false);                
                pInstance.transform.SetParent(transform);
                pInstance.transform.localScale = m_pPrefab.transform.localScale;

                string sName = string.Concat(m_pPrefab.name, " [Spawn] ", "[", pInstance.GetInstanceID(), "]");
                pInstance.name = sName;

                m_lAllInstances.Add(pInstance);

                pPoolableObject = pInstance.GetComponent<IPoolableObject>();
                if (pPoolableObject != null)
                    pPoolableObject.Initialize(this);
            }
        }
        catch (System.Exception e)
        {
            Debug.LogException(e);
        }
    }

    /// <summary>
    /// Destroys the pool along with all their instances and the prefab
    /// </summary>
    public void DestroyPool()
    {
        if (m_lAllInstances != null)
        {
            for (int i = 0; i < m_lAllInstances.Count; i++)
            {
                GameObject pInstance = m_lAllInstances[i];
                if (pInstance != null)
                    Destroy(pInstance);
            }

            m_lAllInstances.Clear();

            IPoolableObject pPoolableObject = m_pPrefab.GetComponent<IPoolableObject>();
            if (pPoolableObject != null)
                pPoolableObject.Initialize(null);
        }
    }

    #endregion

    #region ISpawnPool

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <returns>A spawned copy of the prefab</returns>
    public GameObject Spawn()
    {
        Vector3 vPosition = Vector3.zero;
        Transform pParent = null;
        Quaternion qRotation = Quaternion.identity;
        Vector3 vScale = Vector3.one;
        return Spawn(vPosition, pParent, qRotation, vScale);
    }

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>   
    public GameObject Spawn(Vector3 vPosition)
    {
        Transform pParent = null;
        Quaternion qRotation = Quaternion.identity;
        Vector3 vScale = Vector3.one;
        return Spawn(vPosition, pParent, qRotation, vScale);
    }

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <param name="pParent">Parent of our spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>    
    public GameObject Spawn(Vector3 vPosition, Transform pParent)
    {
        Quaternion qRotation = Quaternion.identity;
        Vector3 vScale = Vector3.one;
        return Spawn(vPosition, pParent, qRotation, vScale);
    }

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <param name="pParent">Parent of our spawned copy</param>
    /// <param name="qRotation">Initial rotation of the spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>
    public GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation)
    {
        Vector3 vScale = Vector3.one;
        return Spawn(vPosition, pParent, qRotation, vScale);
    }

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <param name="pParent">Parent of our spawned copy</param>
    /// <param name="qRotation">Initial rotation of the spawned copy</param>
    /// <param name="vScale">Initial scale of the spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>
    public GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation, Vector3 vScale)
    {
        if (m_lAllInstances == null)
            CreatePool();

        GameObject pInstance = null;
        for (int i = 0; i < m_lAllInstances.Count; i++)
        {
            pInstance = m_lAllInstances[i];
            if (pInstance != null && !pInstance.activeSelf)
            {
                pInstance.SetActive(true);
                pInstance.transform.SetParent(pParent);
                pInstance.transform.rotation = qRotation;
                pInstance.transform.position = vPosition;
                pInstance.transform.localScale = vScale;
                return pInstance;
            }
        }

        pInstance = Instantiate(m_pPrefab, vPosition, qRotation, pParent);
        pInstance.SetActive(true);
        pInstance.transform.SetParent(pParent);
        pInstance.transform.localScale = vScale;

        IPoolableObject pPoolableObject = pInstance.GetComponent<IPoolableObject>();
        if (pPoolableObject != null)
            pPoolableObject.Initialize(this);

        string sName = string.Concat(m_pPrefab.name, " [Spawn] ", "[", pInstance.GetInstanceID(), "]");
        pInstance.name = sName;

        m_lAllInstances.Add(pInstance);

        return pInstance;
    }

    /// <summary>
    /// Disables one of the previously spawned copies of the prefab.
    /// </summary>
    /// <param name="pSpawn">Copy that is going to be recycled</param>
    public void Recycle(GameObject pSpawn)
    {
        if (pSpawn != null && m_lAllInstances != null && m_lAllInstances.Contains(pSpawn))
        {
            pSpawn.transform.position = m_pPrefab.transform.position;
            pSpawn.transform.rotation = m_pPrefab.transform.rotation;
            pSpawn.SetActive(false);
            pSpawn.transform.SetParent(transform);
            pSpawn.transform.localScale = m_pPrefab.transform.localScale;
        }
    }

    #endregion
}
