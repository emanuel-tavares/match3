﻿using System.Collections.Generic;

public class StateMachine<T> : IStateMachine<T>
{
    private IList<IState<T>> m_lStates;
    private IState<T> m_pActiveState;
    private T m_pTarget;

    private StateMachine() { }

    public StateMachine(T pTarget, IList<IState<T>> lStates, IState<T> pActiveState)
    {
        if (pTarget == null)
            throw new System.ArgumentNullException("m_pTarget");

        if (lStates == null)
            throw new System.ArgumentNullException("lStates");

        if (pActiveState == null)
            throw new System.ArgumentNullException("pActiveState");

        if (!lStates.Contains(pActiveState))
            throw new System.ArgumentException("pActiveState must be contained in lStates", "pActiveState");

        m_pTarget = pTarget;
        m_lStates = new List<IState<T>>(lStates);
        m_pActiveState = pActiveState;

        if (m_pActiveState != null)
            m_pActiveState.Begin(m_pTarget, this);
    }

    #region IStateMachine

    public IList<IState<T>> States
    {
        get
        {
            return m_lStates;
        }
    }

    public void AddState(IState<T> pState)
    {
        if (m_lStates != null)
        {
            m_lStates.Add(pState);
        }
    }

    public void UpdateStateMachine(float fDeftaTime)
    {
        if (m_pActiveState != null)
            m_pActiveState.UpdateState(fDeftaTime);
    }

    public void ChangeActiveState(IState<T> pState)
    {
        if (m_pActiveState != null)
            m_pActiveState.End();

        m_pActiveState = null;

        if (pState != null)
        {
            m_pActiveState = pState;
            m_pActiveState.Begin(m_pTarget, this);
        }
    }

    public void RemoveState(IState<T> pState)
    {
        if (m_lStates != null)
        {
            m_lStates.Remove(pState);

            if (m_pActiveState == pState)
                ChangeActiveState(null);
        }
    }

    #endregion
}
