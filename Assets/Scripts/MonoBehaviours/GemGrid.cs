﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GemGrid : MonoBehaviour, ISetGemGrid
{
    // Serializables

    /// <summary>
    /// Number of columns in the IGemGrid
    /// </summary>
    [SerializeField]
    private int m_iNumColumns = 8;

    /// <summary>
    /// Number of lines in the IGemGrid
    /// </summary>
    [SerializeField]
    private int m_iNumLines = 8;

    /// <summary>
    /// Tthe match size in the IGemGrid. This number indicates the minimum number of horizontally or vertically arranged pieces to form a match. Classic games use three.
    /// </summary>
    [SerializeField]
    private int m_iMatchSize = 3;

    /// <summary>
    /// 
    /// </summary>
    [SerializeField]
    private Vector3 m_vGemScale = Vector3.one;

    /// <summary>
    /// Prefabs of every cell that can be instantiated
    /// </summary>
    [SerializeField]
    private GameObject[] m_lCellPrefabs = new GameObject[0];
    
    /// <summary>
    /// GameObject that is supposed to have an ISpawnPoolsManager component attached
    /// </summary>
    [SerializeField]
    private GameObject m_pSpawnPoolsManagerComponent;

    /// <summary>
    /// Depth Mask Prefab
    /// </summary>
    [SerializeField]
    private GameObject m_pDepthMaskPrefab;

    // Non-Serializables
    private ISetCell[,] m_lCells;
    private ISpawnPoolsManager m_pSpawnPoolsManager;

    /// <summary>
    /// Instance of DepthMask that is used to hide the top of the board
    /// </summary>
    private GameObject m_pDepthMaskInstance;

    /// <summary>
    /// State Machine used by GemGrid
    /// </summary>
    private StateMachine<ISetGemGrid> m_pStateMachine;

    #region MonoBehaviour

    protected virtual void OnValidate()
    {
        // Validating m_lCellPrefabs. Its length needs to be higher than zero and every element needs an ICell Component
        if (m_lCellPrefabs == null || m_lCellPrefabs.Length <= 0)
        {
            Debug.LogError("[GemGrid]: m_lCellPrefabPools needs to be populated");
            return;
        }

        for (int i = 0; i < m_lCellPrefabs.Length; i++)
        {
            if (m_lCellPrefabs[i] == null)
            {
                Debug.LogError("[GemGrid]: m_lGemPrefabs cannot have null elements");
                return;
            }

            ICell pCell = m_lCellPrefabs[i].GetComponent<ICell>();
            if (pCell == null)
            {
                Debug.LogError("[GemGrid]: every element in m_lGemPrefabs needs to have an ICell component attached");
                return;
            }
        }

        // Validating m_pSpawnPoolsManagerComponent
        if (m_pSpawnPoolsManagerComponent == null)
        {
            Debug.LogError("[GemGrid]: m_pSpawnPoolsManagerComponent cannot be null");
            return;
        }

        m_pSpawnPoolsManager = m_pSpawnPoolsManagerComponent.GetComponent<ISpawnPoolsManager>();

        if (m_pSpawnPoolsManager == null)
        {
            Debug.LogError("[GemGrid]: m_pSpawnPoolsManagerComponent needs to have an ISpawnPoolsManager component attached");
            return;
        }

        // Validating m_pDepthMaskPrefab
        if (m_pDepthMaskPrefab == null)
        {
            Debug.LogError("[GemGrid]: m_pDepthMask cannot be null");
            return;
        }
    }

    protected virtual void OnEnable()
    {
        GemGridBuildState pInitialActiveState = GetComponent<GemGridBuildState>();
        if (m_pStateMachine == null)
        {
            // Initializing StateMachine. First active machine is GemGridBuildState
            List<IState<ISetGemGrid>> lStates = new List<IState<ISetGemGrid>>();
            lStates.Add(pInitialActiveState);
            m_pStateMachine = new StateMachine<ISetGemGrid>(this, lStates, pInitialActiveState);
        }
        else
        {
            m_pStateMachine.ChangeActiveState(pInitialActiveState);
        }
    }
    
    protected virtual void Update()
    {
        m_pStateMachine.UpdateStateMachine(Time.deltaTime);
    }

    protected virtual void OnDisable()
    {
        if (m_pDepthMaskInstance != null && m_pSpawnPoolsManager != null)
        {
            // We recycle the DepthMask instance when the GemGrid is disabled
            m_pSpawnPoolsManager.Recycle(m_pDepthMaskInstance);
            m_pDepthMaskInstance = null;
        }
    }

    #endregion

    #region IGemGrid

    /// <summary>
    /// Removes every gem from the board.
    /// </summary>
    public void RemoveAllGems()
    {
        if (m_lCells != null)
        {
            int iDoubleNumLines = NumLines * 2;
            for (int i = 0; i < NumColumns; i++)
            {
                for (int j = 0; j < iDoubleNumLines; j++)
                    RemoveGem(i, j);
            }
        }
    }

    /// <summary>
    /// Builds an (NumColumns, NumLines) board with empty ICells. Filling the board with gems will come later.
    /// </summary>
    public void DisableBoard()
    {
        if (m_lCells != null)
        {
            int iCellIndex = 0;
            int iDoubleNumLines = NumLines * 2;
            for (int i = 0; i < NumColumns; i++)
            {
                for (int j = 0; j < iDoubleNumLines; j++)
                {
                    m_pSpawnPoolsManager.Recycle(m_lCells[i, j].GameObject);
                    iCellIndex = (iCellIndex + 1) % m_lCellPrefabs.Length;
                }
                iCellIndex = (iCellIndex + 1) % m_lCellPrefabs.Length;
            }
        }
    }

    public void BuildBoard()
    {
        int iDoubleNumLines = NumLines * 2;

        m_lCells = new ISetCell[NumColumns, iDoubleNumLines];

        float x = -GemScale.x * ((NumColumns - 1) * 0.5f);
        float z = 0f;

        int iCellIndex = 0;

        for (int i = 0; i < NumColumns; i++)
        {
            float y = (NumLines * 0.5f) + (NumLines - 1) + (GemScale.y * 0.5f);

            for (int j = 0; j < iDoubleNumLines; j++)
            {
                Vector3 vGemPosition = transform.TransformPoint(new Vector3(x, y, z));
                GameObject pCellPrefab = m_lCellPrefabs[iCellIndex];
                GameObject pCell = m_pSpawnPoolsManager.Spawn(pCellPrefab, transform.position, transform, transform.rotation, GemScale);
                pCell.transform.localPosition = vGemPosition;
                m_lCells[i, j] = pCell.GetComponent<ISetCell>();
                m_lCells[i, j].SetupCell(i, j, vGemPosition);

                y -= GemScale.y;

                iCellIndex = (iCellIndex + 1) % m_lCellPrefabs.Length;
            }

            x += GemScale.x;

            iCellIndex = (iCellIndex + 1) % m_lCellPrefabs.Length;
        }

        // Inserting Depth Mask to hide the top of the board
        if (m_pDepthMaskInstance == null)
        {
            Vector3 vDepthMaskScale = Vector3.one;
            vDepthMaskScale.x = GemScale.x * NumColumns;
            vDepthMaskScale.y = GemScale.y * NumLines;
            vDepthMaskScale.z = m_pDepthMaskPrefab.transform.localScale.z;

            Vector3 vDepthMaskPos = Vector3.zero;
            vDepthMaskPos.x = transform.position.x;
            vDepthMaskPos.y = transform.position.y + vDepthMaskScale.y;
            vDepthMaskPos.z = transform.position.z - 1;
            vDepthMaskPos = transform.TransformPoint(vDepthMaskPos);

            m_pDepthMaskInstance = m_pSpawnPoolsManager.Spawn(m_pDepthMaskPrefab, vDepthMaskPos, transform, transform.rotation, vDepthMaskScale);
        }
    }

    /// <summary>
    /// Builds an (NumColumns, NumLines) board with empty ICells. Filling the board with gems will come later.
    /// </summary>
    /// <param name="iNumColumns">Number of columns of the new Board</param>
    /// <param name="iNumLines">Number of lines of the new Board</param>
    public void BuildBoard(int iNumColumns, int iNumLines)
    {
        BuildBoard(iNumColumns, iNumLines, GemScale);
    }

    /// <summary>
    /// Builds an (NumColumns, NumLines) board with empty ICells. Filling the board with gems will come later.
    /// </summary>
    /// <param name="iNumColumns">Number of columns of the new Board</param>
    /// <param name="iNumLines">Number of lines of the new Board</param>
    /// <param name="vGemScale">Gems scale from the Board</param>
    public void BuildBoard(int iNumColumns, int iNumLines, Vector3 vGemScale)
    {
        m_iNumColumns = iNumColumns;
        m_iNumLines = iNumLines;
        m_vGemScale = vGemScale;
        BuildBoard();
    }

    /// <summary>
    /// Returns the ICell located in the (iColumn, iLine) space.
    /// </summary>
    public ICell this[int iColumn, int iLine]
    {
        get
        {
            return m_lCells[iColumn, iLine];
        }
    }

    /// <summary>
    /// Sets a pGem in the following iColumn and iLine on the Grid
    /// </summary>
    /// <param name="iColumn">Column where the new gem will be added</param>
    /// <param name="iLine">Line where the new gem will be added</param>
    /// <param name="pGem">The gem that is going to be added</param>
    /// <returns>True if the new gem was added</returns>
    public bool AddGem(int iColumn, int iLine, IGem pGem)
    {
        if (m_lCells != null)
        {
            ISetCell pCell = m_lCells[iColumn, iLine];
            pCell.ChangeGem(pGem);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Removes the gem from the Cell located in (iColumn, iLine). If bDestroyCurrent is set to true, it destroys the current gem after removal
    /// </summary>
    /// <param name="iColumn">Column from where the new gem will be removed</param>
    /// <param name="iLine">Line from where the new gem will be removed</param>
    /// <param name="bDestroyCurrent">If bDestroyCurrent is set to true, it destroys the current gem after removal</param>
    /// <returns>True if the gem was successfully removed</returns>
    public bool RemoveGem(int iColumn, int iLine, bool bDestroyCurrent = true)
    {
        if (m_lCells != null)
        {
            ISetCell pCell = m_lCells[iColumn, iLine];
            pCell.ChangeGem(null, bDestroyCurrent);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Swaps the gem values between (iColumn1, iLine1) and (iColumn2, iLine2)
    /// </summary>
    public void SwapGems(int iColumn1, int iLine1, int iColumn2, int iLine2)
    {
        if (m_lCells != null)
        {
            ISetCell pCell1 = m_lCells[iColumn1, iLine1];
            ISetCell pCell2 = m_lCells[iColumn2, iLine2];
            IGem pGem1 = pCell1.Gem;
            IGem pGem2 = pCell2.Gem;

            // if bDestroyCurrent was set to true, the gem would be destroyed before it was swapped
            pCell1.ChangeGem(pGem2, false); 
            pCell2.ChangeGem(pGem1, false);
        }
    }

    /// <summary>
    /// Returns the scale of the gem, in lossy values.
    /// </summary>
    public Vector3 GemScale
    {
        get
        {
            return m_vGemScale;
        }
    }

    /// <summary>
    /// Returns the number of columns in the IGemGrid
    /// </summary>
    public int NumColumns
    {
        get
        {
            return m_iNumColumns;
        }
    }

    /// <summary>
    /// Returns the number of lines in the IGemGrid
    /// </summary>
    public int NumLines
    {
        get
        {
            return m_iNumLines;
        }
    }

    /// <summary>
    /// Returns the match size in the IGemGrid. This number indicates the minimum number of horizontally or vertically arranged pieces to form a match. Classic games use three.
    /// </summary>
    public int MatchSize
    {
        get
        {
            return m_iMatchSize;
        }
    }

    #endregion
}
