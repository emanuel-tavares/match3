﻿using System;
using UnityEngine;
using UnityEngine.Events;

public class Gem : MonoBehaviour, IGem
{
    // Serializables

    /// <summary>
    /// Entry component that is supposed to have an IGemType attached
    /// </summary>
    [SerializeField]
    private Component m_pGemTypeComponent;

    // Non-Serializables

    /// <summary>
    /// Stores the IGemType component of this Gem
    /// </summary>
    private IGemType m_pGemType;

    #region MonoBehaviour

    protected virtual void OnValidate()
    {
        // Validating IGemType. It is valid if m_pGemTypeComponent has a IGemType component attached.
        if (m_pGemTypeComponent == null)
        {
            m_pGemTypeComponent = GetComponent(typeof(IGemType));

            if (m_pGemTypeComponent == null)
            {
                Debug.LogError("[Gem]: m_pGemTypeComponent cannot be null");
                return;
            }
        }

        m_pGemType = m_pGemTypeComponent.GetComponent<IGemType>();

        if (m_pGemType == null)
        {
            Debug.LogError("[Gem]: m_pGemTypeComponent needs to have an IGemType component attached");
        }
    }

    #endregion

    #region IGem

    /// <summary>
    /// Returns the gem type
    /// </summary>
    public IGemType Type
    {
        get
        {
            if (m_pGemType == null)
                m_pGemType = m_pGemTypeComponent.GetComponent<IGemType>();
            return m_pGemType;
        }
    }

    /// <summary>
    /// Returns the GameObject of this IGem
    /// </summary>
    public GameObject GameObject
    {
        get
        {
            return gameObject;
        }
    }

    /// <summary>
    /// Returns true if both gems have the same type.
    /// </summary>
    /// <param name="pGem">Gem that is going to be used for comparison</param>
    /// <returns>Returns true if both gems have the same type.</returns>
    public bool IsSameType(IGem pGem)
    {
        return Type.Equals(pGem.Type);
    }

    #endregion
}

