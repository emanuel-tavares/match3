﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class GemGridHandleInputState : MonoBehaviour, IState<ISetGemGrid>
{
    // Serializables
    [SerializeField]
    private float m_fMaxDragDistance = 1f;

    // Non-Serializables
    private IStateMachine<ISetGemGrid> m_pStateMachine;
    private ISetGemGrid m_pGemGrid;
    private UnityAction<ICell, Vector3> m_pOnDragAction;
    private UnityAction<ICell, Vector3> m_pOnReleaseAction;
    private Vector3? m_vStartDrag;

    #region IState

    public void Begin(ISetGemGrid pTarget, IStateMachine<ISetGemGrid> pStateMachine)
    {
        m_vStartDrag = null;
        m_pGemGrid = pTarget;
        m_pStateMachine = pStateMachine;

        m_pOnDragAction = new UnityAction<ICell, Vector3>(OnDrag);
        m_pOnReleaseAction = new UnityAction<ICell, Vector3>(OnRelease);

        int iDoubleNumLines = m_pGemGrid.NumLines * 2;
        for (int i = 0; i < m_pGemGrid.NumColumns; i++)
        {
            for (int j = m_pGemGrid.NumLines; j < iDoubleNumLines; j++)
            {
                ICell pCell = m_pGemGrid[i, j];
                pCell.AddDragListener(m_pOnDragAction);
                pCell.AddReleaseListener(m_pOnReleaseAction);
            }
        }
    }

    public void UpdateState(float fDeltaTime) { }

    public void End()
    {
        int iDoubleNumLines = m_pGemGrid.NumLines * 2;
        for (int i = 0; i < m_pGemGrid.NumColumns; i++)
        {
            for (int j = m_pGemGrid.NumLines; j < iDoubleNumLines; j++)
            {
                ICell pCell = m_pGemGrid[i, j];
                pCell.RemoveDragListener(m_pOnDragAction);
                pCell.RemoveReleaseListener(m_pOnReleaseAction);
            }
        }

        m_pOnDragAction = null;
        m_pOnReleaseAction = null;
    }

    #endregion

    #region GemGridHandleInputState

    private void SwapCells(ICell pTargetCell, Vector3 vDragDirection)
    {
        ICell pNextCell = default(ICell);
        IGem pTargetGem = pTargetCell.Gem;

        int iColumn = pTargetCell.Column;
        int iLine = pTargetCell.Line;
        int iDoubleNumLines = m_pGemGrid.NumLines * 2;
        if (Mathf.Abs(vDragDirection.x) > Mathf.Abs(vDragDirection.y))
        {
            if (vDragDirection.x > 0 && iColumn + 1 < m_pGemGrid.NumColumns) // right
                pNextCell = m_pGemGrid[iColumn + 1, iLine];
            else if (vDragDirection.x < 0 && iColumn - 1 >= 0) // left
                pNextCell = m_pGemGrid[iColumn - 1, iLine];
        }
        else
        {
            if (vDragDirection.y > 0 && iLine - 1 >= m_pGemGrid.NumLines) // up
                pNextCell = m_pGemGrid[iColumn, iLine - 1];
            else if (vDragDirection.y < 0 && iLine + 1 < iDoubleNumLines)  // down
                pNextCell = m_pGemGrid[iColumn, iLine + 1];
        }

        if (pNextCell != null)
        {
            GemGridSwapCellsState pState = GetComponent<GemGridSwapCellsState>();
            pState.Initialize(pTargetCell, pNextCell);
            m_pStateMachine.ChangeActiveState(pState);
        }
    }

    private void OnDrag(ICell pTargetGem, Vector3 vMousePosition)
    {
        Vector3 vMousePosTransformed = Camera.main.ScreenToWorldPoint(vMousePosition); // this works because main camera is orthographic
        if (!m_vStartDrag.HasValue)
        {
            m_vStartDrag = vMousePosTransformed;
            return;
        }
    }

    private void OnRelease(ICell pTargetCell, Vector3 vMousePosition)
    {
        if (m_vStartDrag.HasValue)
        {
            Vector3 vMousePosTransformed = Camera.main.ScreenToWorldPoint(vMousePosition); // this works because main camera is orthographic
            Vector3 vDragDirection = vMousePosTransformed - m_vStartDrag.Value;
            if (vDragDirection.magnitude >= m_fMaxDragDistance)
                SwapCells(pTargetCell, vDragDirection);

            m_vStartDrag = null;
        }
    }

    #endregion
}
