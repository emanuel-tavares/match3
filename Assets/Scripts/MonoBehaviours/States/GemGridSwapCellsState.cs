﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemGridSwapCellsState : MonoBehaviour, IState<ISetGemGrid>
{
    // Serializable
    [Header("Swap Animation")]
    [SerializeField]
    private AnimationCurve m_pSwapAnimationCurve;
    [SerializeField]
    private float m_fSwapTotalTime = 0.25f;
    [Header("Clear Match Animation")]
    [SerializeField]
    private AnimationCurve m_pClearMatchAnimationCurve;
    [SerializeField]
    private float m_fClearMatchTotalTime = 0.25f;

    // Non-Serializable
    private ISetGemGrid m_pGemGrid;
    private IStateMachine<ISetGemGrid> m_pStateMachine;
    private ICell m_pSwappedCell1;
    private ICell m_pSwappedCell2;

    public bool Initialized { get; private set; }

    #region IState

    public void Begin(ISetGemGrid pTarget, IStateMachine<ISetGemGrid> pStateMachine)
    {
        if (!Initialized)
        {
            Debug.LogError("[GemGridSwapCellsState]: This state must be properly initialized. Use the initialize() method before setting it as an active state.");
            return;
        }

        m_pGemGrid = pTarget;
        m_pStateMachine = pStateMachine;

        StartCoroutine(SwapGemsCoroutine());
    }

    public void UpdateState(float fDeltaTime)
    {

    }

    public void End()
    {
        Initialized = false;
        m_pSwappedCell1 = null;
        m_pSwappedCell2 = null;
    }

    #endregion

    #region GemGridSwapCellsState

    public void Initialize(ICell pSwappedCell1, ICell pSwappedCell2)
    {
        if (!Initialized)
        {
            m_pSwappedCell1 = pSwappedCell1;
            m_pSwappedCell2 = pSwappedCell2;
        }
        Initialized = true;
    }

    private bool HasMatched(ICell pCell, Vector3 vMatchDirection)
    {
        return false;
    }

    private IEnumerator SwapGemsCoroutine()
    {
        Vector3 vSwapDirection = Vector3.zero;
        Match pMatch1 = null;
        Match pMatch2 = null;
        yield return SwapGemsAnimation(m_pSwappedCell1, m_pSwappedCell2);

        vSwapDirection = m_pSwappedCell1.GameObject.transform.position - m_pSwappedCell2.GameObject.transform.position;
        pMatch1 = GetMatchAfterSwap(m_pSwappedCell1, vSwapDirection);

        vSwapDirection = m_pSwappedCell2.GameObject.transform.position - m_pSwappedCell1.GameObject.transform.position;
        pMatch2 = GetMatchAfterSwap(m_pSwappedCell2, vSwapDirection);

        if (pMatch1 == null && pMatch2 == null)
        {
            yield return SwapGemsAnimation(m_pSwappedCell2, m_pSwappedCell1);

            IState<ISetGemGrid> pState = GetComponent<GemGridHandleInputState>();
            m_pStateMachine.ChangeActiveState(pState);
        }
        else
        {
            StartCoroutine(ClearMatchAnimation(pMatch1));
            StartCoroutine(ClearMatchAnimation(pMatch2));
        }
    }

    private IEnumerator SwapGemsAnimation(ICell pFrom, ICell pTo)
    {
        Vector3 vGem1Pos = pFrom.GameObject.transform.position;
        Vector3 vGem2Pos = pTo.GameObject.transform.position;
        IGem pGem1 = pFrom.Gem;
        IGem pGem2 = pTo.Gem;
        for (float fTime = 0f; fTime < m_fSwapTotalTime; fTime += Time.deltaTime)
        {
            if (pGem1 != null)
            {
                Vector3 vNextPosition = Vector3.Lerp(vGem1Pos, vGem2Pos, m_pSwapAnimationCurve.Evaluate(fTime / m_fSwapTotalTime));
                pGem1.GameObject.transform.position = vNextPosition;
            }

            if (pGem2 != null)
            {
                Vector3 vNextPosition = Vector3.Lerp(vGem2Pos, vGem1Pos, m_pSwapAnimationCurve.Evaluate(fTime / m_fSwapTotalTime));
                pGem2.GameObject.transform.position = vNextPosition;
            }
            yield return null;
        }

        m_pGemGrid.SwapGems(pFrom.Column, pFrom.Line, pTo.Column, pTo.Line);
    }

    private Match GetMatchAfterSwap(ICell pSwap, Vector3 vSwapDirection)
    {        
        HashSet<ICell> lHorizontalCellsToRemove = new HashSet<ICell>();
        HashSet<ICell> lVerticalCellsToRemove = new HashSet<ICell>();

        IGem pFirstGem = pSwap.Gem;
        int iColumn = pSwap.Column;
        int iLine = pSwap.Line;

        lHorizontalCellsToRemove.Add(pSwap);
        lVerticalCellsToRemove.Add(pSwap);

        if (vSwapDirection != Vector3.left)
        {
            int iMaxColumn = Mathf.Min(iColumn + m_pGemGrid.MatchSize, m_pGemGrid.NumColumns);
            for (int i = iColumn + 1; i < iMaxColumn; i++)
            {
                IGem pOtherGem = m_pGemGrid[i, iLine].Gem;
                if (pOtherGem != null && pOtherGem.IsSameType(pFirstGem))
                    lHorizontalCellsToRemove.Add(m_pGemGrid[i, iLine]);
                else
                    break;
            }
        }

        if (vSwapDirection != Vector3.right)
        {
            int iMinColumn = Mathf.Max(iColumn - m_pGemGrid.MatchSize, 0);
            for (int i = iColumn - 1; i >= iMinColumn; i--)
            {
                IGem pOtherGem = m_pGemGrid[i, iLine].Gem;
                if (pOtherGem != null && pOtherGem.IsSameType(pFirstGem))
                    lHorizontalCellsToRemove.Add(m_pGemGrid[i, iLine]);
                else
                    break;
            }
        }

        if (vSwapDirection != Vector3.up)
        {
            int iMaxLine = Mathf.Min(iLine + m_pGemGrid.MatchSize, m_pGemGrid.NumLines * 2);
            for (int j = iLine + 1; j < iMaxLine; j++)
            {
                IGem pOtherGem = m_pGemGrid[iColumn, j].Gem;
                if (pOtherGem != null && pOtherGem.IsSameType(pFirstGem))
                    lVerticalCellsToRemove.Add(m_pGemGrid[iColumn, j]);
                else
                    break;
            }
        }

        if (vSwapDirection != Vector3.down)
        {
            int iMinLine = Mathf.Max(iLine - m_pGemGrid.MatchSize, m_pGemGrid.NumLines);
            for (int j = iLine - 1; j >= iMinLine; j--)
            {
                IGem pOtherGem = m_pGemGrid[iColumn, j].Gem;
                if (pOtherGem != null && pOtherGem.IsSameType(pFirstGem))
                    lVerticalCellsToRemove.Add(m_pGemGrid[iColumn, j]);
                else
                    break;
            }
        }

        HashSet<ICell> lCellsToRemove = new HashSet<ICell>();
        if (lVerticalCellsToRemove.Count >= m_pGemGrid.MatchSize)
            lCellsToRemove.UnionWith(lVerticalCellsToRemove);
        
        if (lHorizontalCellsToRemove.Count >= m_pGemGrid.MatchSize)
            lCellsToRemove.UnionWith(lHorizontalCellsToRemove);

        if (lCellsToRemove.Count > 0)
            return new Match(lHorizontalCellsToRemove.Count, lVerticalCellsToRemove.Count, lCellsToRemove);

        return null;
    }

    private IEnumerator ClearMatchAnimation(Match pMatch)
    {
        if (pMatch != null)
        {
            for (float fElapsedTime = 0f; fElapsedTime < m_fClearMatchTotalTime; fElapsedTime += Time.deltaTime)
            {
                foreach (ICell pCell in pMatch.MatchedCells)
                {
                    IGem pGem = pCell.Gem;
                    if (pGem != null)
                    {
                        Vector3 vFirstLocalScale = pCell.GameObject.transform.localScale;
                        pGem.GameObject.transform.localScale = Vector3.Lerp(vFirstLocalScale, Vector3.zero, m_pClearMatchAnimationCurve.Evaluate(fElapsedTime / m_fClearMatchTotalTime));
                        yield return null;
                    }
                }
            }

            foreach (ICell pCell in pMatch.MatchedCells)
            {
                IGem pGem = pCell.Gem;
                if (pGem != null)
                    m_pGemGrid.RemoveGem(pCell.Column, pCell.Line);
            }
        }
    }

    #endregion
}
