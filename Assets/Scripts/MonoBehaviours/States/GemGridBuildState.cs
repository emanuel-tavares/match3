﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemGridBuildState : MonoBehaviour, IState<ISetGemGrid>
{
    // Serializables
    [SerializeField]
    private GameObject[] m_lGemPrefabsComponent;
    [SerializeField]
    private GameObject m_pSpawnPoolsManagerComponent;

    // Non-Serializables
    private ISetGemGrid m_pGemGrid;
    private ISpawnPoolsManager m_pSpawnPoolsManager;
    private List<IGem> m_lGemPrefabs;

    #region MonoBehaviour

    protected virtual void OnValidate()
    {
        // Validating m_lGemPrefabsComponent. Its length needs to be higher than zero and every element needs an IGem Component
        if (m_lGemPrefabsComponent == null || m_lGemPrefabsComponent.Length <= 0)
        {
            Debug.LogError("[GemGrid]: m_lGemPrefabsComponent needs to be populated");
            return;
        }

        for (int i = 0; i < m_lGemPrefabsComponent.Length; i++)
        {
            if (m_lGemPrefabsComponent[i] == null)
            {
                Debug.LogError("[GemGrid]: m_lGemPrefabsComponent cannot have null elements");
                return;
            }

            IGem pGem = m_lGemPrefabsComponent[i].GetComponent<IGem>();
            if (pGem == null)
            {
                Debug.LogError("[GemGrid]: Every element in m_lGemPrefabsComponent must have an IGem component");
                return;
            }
        }

        // Validating m_pSpawnPoolsManagerComponent
        if (m_pSpawnPoolsManagerComponent == null)
        {
            Debug.LogError("[GemGridBuilder]: m_pSpawnPoolsManagerComponent cannot be null");
            return;
        }

        m_pSpawnPoolsManager = m_pSpawnPoolsManagerComponent.GetComponent<ISpawnPoolsManager>();

        if (m_pSpawnPoolsManager == null)
        {
            Debug.LogError("[GemGridBuilder]: m_pSpawnPoolsManagerComponent needs to have an ISpawnPoolsManager component attached");
            return;
        }
    }

    #endregion

    #region GemGridBuildState

    private void CreatePrefabsList()
    {
        if (m_lGemPrefabs != null)
            m_lGemPrefabs.Clear();

        m_lGemPrefabs = new List<IGem>(m_lGemPrefabsComponent.Length);

        for (int i = 0; i < m_lGemPrefabsComponent.Length; i++)
        {
            IGem pGem = m_lGemPrefabsComponent[i].GetComponent<IGem>();
            m_lGemPrefabs.Add(pGem);
        }
    }

    private void FillBoard()
    {
        for (int iColumn = 0; iColumn < m_pGemGrid.NumColumns; iColumn++)
        {
            for (int iLine = 0; iLine < m_pGemGrid.NumLines; iLine++)
            {
                List<IGem> lAllowedGems = new List<IGem>(m_lGemPrefabs);
                IGem pFirstGem = null;
                int iEqualStreak = 0;
                int iMinColumn = Mathf.Max(iColumn - m_pGemGrid.MatchSize, 0);
                for (int i = iColumn - 1; i >= iMinColumn; i--)
                {
                    ICell pCell = m_pGemGrid[i, iLine];
                    if (pFirstGem == null)
                    {
                        pFirstGem = pCell.Gem;
                        continue;
                    }

                    if (pCell.Gem.IsSameType(pFirstGem))
                        iEqualStreak++;
                    else
                        break;
                }

                if (iEqualStreak >= (m_pGemGrid.MatchSize - 2))
                {
                    for (int i = 0; i < lAllowedGems.Count; i++)
                    {
                        if (lAllowedGems[i].IsSameType(pFirstGem))
                        {
                            lAllowedGems.RemoveAt(i);
                            break;
                        }
                    }
                }

                pFirstGem = null;
                iEqualStreak = 0;
                int iMinLine = Mathf.Max(iLine - m_pGemGrid.MatchSize, 0);
                for (int j = iLine - 1; j >= iMinLine; j--)
                {
                    ICell pCell = m_pGemGrid[iColumn, j];
                    if (pFirstGem == null)
                    {
                        pFirstGem = pCell.Gem;
                        continue;
                    }

                    if (pCell.Gem.IsSameType(pFirstGem))
                        iEqualStreak++;
                    else
                        break;
                }

                if (iEqualStreak >= (m_pGemGrid.MatchSize - 2))
                {
                    for (int i = 0; i < lAllowedGems.Count; i++)
                    {
                        if (lAllowedGems[i].IsSameType(pFirstGem))
                        {
                            lAllowedGems.RemoveAt(i);
                            break;
                        }
                    }
                }

                IGem pChosenGemPrefab = lAllowedGems.GetRandomValue();
                if (pChosenGemPrefab != null)
                {
                    IGem pSpawnedGem = m_pSpawnPoolsManager.Spawn(pChosenGemPrefab.GameObject).GetComponent<IGem>();
                    m_pGemGrid.AddGem(iColumn, iLine, pSpawnedGem);
                }
            }
        }
    }

    private bool HasAnyMatches(int iStartColumn, int iStartLine, int iEndColumn, int iEndLine)
    {
        for (int iColumn = iStartColumn; iColumn < iEndColumn; iColumn++)
        {
            for (int iLine = iStartLine; iLine < iEndLine; iLine++)
            {
                IGem pFirstGem = m_pGemGrid[iColumn, iLine].Gem;
                if (pFirstGem != null)
                {
                    bool bStillOnStreak = true;
                    int iEqualStreak = 0;
                    int iMinColumn = Mathf.Max(iColumn - m_pGemGrid.MatchSize, iStartColumn);
                    for (int i = iColumn - 1; i >= iMinColumn; i--)
                    {
                        ICell pCell = m_pGemGrid[i, iLine];
                        if (pCell.Gem.IsSameType(pFirstGem))
                        {
                            iEqualStreak++;
                            continue;
                        }
                        else if (bStillOnStreak)
                        {
                            if (iLine + 1 < iEndLine)
                            {
                                pCell = m_pGemGrid[i, iLine + 1];
                                if (pCell.Gem.IsSameType(pFirstGem))
                                {
                                    iEqualStreak++;
                                    bStillOnStreak = false;
                                    continue;
                                }
                            }

                            if (iLine - 1 >= iStartLine)
                            {
                                pCell = m_pGemGrid[i, iLine - 1];
                                if (pCell.Gem.IsSameType(pFirstGem))
                                {
                                    iEqualStreak++;
                                    bStillOnStreak = false;
                                    continue;
                                }
                            }
                        }

                        break;
                    }

                    if (iEqualStreak >= (m_pGemGrid.MatchSize - 1))
                        return true;

                    bStillOnStreak = true;
                    iEqualStreak = 0;
                    int iMinLine = Mathf.Max(iLine - m_pGemGrid.MatchSize, iStartLine);
                    for (int j = iLine - 1; j >= iMinLine; j--)
                    {
                        ICell pCell = m_pGemGrid[iColumn, j];
                        if (pCell.Gem.IsSameType(pFirstGem))
                        {
                            iEqualStreak++;
                            continue;
                        }
                        else if (bStillOnStreak)
                        {
                            if (iColumn + 1 < iEndColumn)
                            {
                                pCell = m_pGemGrid[iColumn + 1, j];
                                if (pCell.Gem.IsSameType(pFirstGem))
                                {
                                    iEqualStreak++;
                                    bStillOnStreak = false;
                                    continue;
                                }
                            }

                            if (iColumn - 1 >= iStartColumn)
                            {
                                pCell = m_pGemGrid[iColumn - 1, j];
                                if (pCell.Gem.IsSameType(pFirstGem))
                                {
                                    iEqualStreak++;
                                    bStillOnStreak = false;
                                    continue;
                                }
                            }
                        }

                        break;
                    }

                    if (iEqualStreak >= (m_pGemGrid.MatchSize - 1))
                        return true;
                }
            }
        }
        return false;
    }

    #endregion

    #region IState

    public void Begin(ISetGemGrid pTarget, IStateMachine<ISetGemGrid> pStateMachine)
    {
        m_pGemGrid = pTarget;

        m_pGemGrid.DisableBoard();
        m_pGemGrid.BuildBoard();

        CreatePrefabsList();

        FillBoard();

        while (!HasAnyMatches(0, 0, m_pGemGrid.NumColumns, m_pGemGrid.NumLines))
        {
            Debug.Log("No matches! Redo!");
            FillBoard();
        }

        IState<ISetGemGrid> pState = gameObject.GetComponent<GemGridFallState>();
        pStateMachine.AddState(pState);
        pStateMachine.ChangeActiveState(pState);
    }

    public void UpdateState(float fDeltaTime) { }

    public void End() { }

    #endregion

}
