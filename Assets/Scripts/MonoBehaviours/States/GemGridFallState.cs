﻿using System.Collections;
using UnityEngine;

public class GemGridFallState : MonoBehaviour, IState<ISetGemGrid>
{
    // Serializables
    [SerializeField]
    private AnimationCurve m_pAnimationCurve;
    [SerializeField]
    private float m_fTotalTime = 1.5f;

    // Non-Serializables
    private IStateMachine<ISetGemGrid> m_pStateMachine;
    private ISetGemGrid m_pGemGrid;
    private float m_fElapsedTime = 0f;

    #region IState

    public void Begin(ISetGemGrid pTarget, IStateMachine<ISetGemGrid> pStateMachine)
    {
        m_fElapsedTime = 0f;
        m_pGemGrid = pTarget;
        m_pStateMachine = pStateMachine;
    }

    public void UpdateState(float fDeltaTime)
    {
        float fTime = m_pAnimationCurve.Evaluate(m_fElapsedTime / m_fTotalTime);

        for (int iColumn = 0; iColumn < m_pGemGrid.NumColumns; iColumn++)
        {
            for (int iLine = 0; iLine < m_pGemGrid.NumLines; iLine++)
            {
                ICell pCell = m_pGemGrid[iColumn, iLine];
                ICell pTargetCell = m_pGemGrid[iColumn, iLine + m_pGemGrid.NumLines];

                if (pCell.Gem != null)
                {
                    GameObject pGemGameObject = pCell.Gem.GameObject;
                    Vector3 vStartPosition = pCell.GameObject.transform.position;
                    Vector3 vEndPosition = pTargetCell.GameObject.transform.position;

                    vStartPosition = Vector3.Lerp(vStartPosition, vEndPosition, fTime);
                    pGemGameObject.transform.position = vStartPosition;
                }
            }
        }

        if (m_fElapsedTime < m_fTotalTime)
            m_fElapsedTime = Mathf.MoveTowards(m_fElapsedTime, m_fTotalTime, Time.deltaTime);
        else
        {
            // finish
            IState<ISetGemGrid> pState = GetComponent<GemGridHandleInputState>();
            m_pStateMachine.ChangeActiveState(pState);
        }
        
    }

    public void End()
    {
        for (int iColumn = 0; iColumn < m_pGemGrid.NumColumns; iColumn++)
        {
            for (int iLine = 0; iLine < m_pGemGrid.NumLines; iLine++)
            {
                ICell pCell = m_pGemGrid[iColumn, iLine];
                ICell pTargetCell = m_pGemGrid[iColumn, iLine + m_pGemGrid.NumLines];

                if (pCell.Gem != null)
                    m_pGemGrid.SwapGems(iColumn, iLine, pTargetCell.Column, pTargetCell.Line);
            }
        }
    }

    #endregion
}
