﻿using UnityEngine;

/// <summary>
/// Writable base interface for the Cells in IGemGrid.
/// </summary>
public interface ISetCell : ICell
{
    /// <summary>
    /// Initialization setup. Defines a column, a line and a global world position for the ICell. You can also insert a IGem right away.
    /// </summary>
    /// <param name="iColumn">Column of the cell</param>
    /// <param name="iLine">Line of the cell</param>
    /// <param name="vPosition">World position of the cell</param>
    /// <param name="pGem">Gem of the cell</param>
    void SetupCell(int iColumn, int iLine, Vector3 vPosition, IGem pGem = null);

    /// <summary>
    /// Changes the current gem being hold by the ICell. If bDestroyCurrent is true, the current gem is recycled or destroyed before it is changed by pGem.
    /// </summary>
    /// <param name="pGem">Gem that is going to substitute the current gem</param>
    /// <param name="bDestroyCurrent">If true the current gem will be either destroyed or recycled</param>
    void ChangeGem(IGem pGem, bool bDestroyCurrent = true);
}
