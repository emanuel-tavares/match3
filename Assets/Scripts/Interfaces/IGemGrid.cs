﻿using UnityEngine;

/// <summary>
/// This is our base interface for our Match 3 grid. It is intended to be a readable version of it. 
/// It is supposed to be an (N, N) grid formed by ICells. Each ICell can contain an IGem.
/// </summary>
public interface IGemGrid
{
    /// <summary>
    /// Returns the number of columns in the IGemGrid
    /// </summary>
    int NumColumns { get; }

    /// <summary>
    /// Returns the number of lines in the IGemGrid
    /// </summary>
    int NumLines { get; }

    /// <summary>
    /// Returns the match size in the IGemGrid. This number indicates the minimum number of horizontally or vertically arranged pieces to form a match. Classic games use three.
    /// </summary>
    int MatchSize { get; }

    /// <summary>
    /// Returns the scale of the gem, in lossy values.
    /// </summary>
    Vector3 GemScale { get; }

    /// <summary>
    /// Returns the ICell located in the (iColumn, iLine) space.
    /// </summary>
    ICell this[int iColumn, int iLine] { get; }

    /// <summary>
    /// Builds an (NumColumns, NumLines) board with empty ICells. Filling the board with gems will come later.
    /// </summary>
    void BuildBoard();

    /// <summary>
    /// Builds an (NumColumns, NumLines) board with empty ICells. Filling the board with gems will come later.
    /// </summary>
    /// <param name="iNumColumns">Number of columns of the new Board</param>
    /// <param name="iNumLines">Number of lines of the new Board</param>
    void BuildBoard(int iNumColumns, int iNumLines);

    /// <summary>
    /// Builds an (NumColumns, NumLines) board with empty ICells. Filling the board with gems will come later.
    /// </summary>
    /// <param name="iNumColumns">Number of columns of the new Board</param>
    /// <param name="iNumLines">Number of lines of the new Board</param>
    /// <param name="vGemScale">Gems scale from the Board</param>
    void BuildBoard(int iNumColumns, int iNumLines, Vector3 vGemScale);

    /// <summary>
    /// Empties the board and destroy all the ICells, along with their respectives IGems.
    /// </summary>
    void DisableBoard();

    /// <summary>
    /// Removes every gem from the board.
    /// </summary>
    void RemoveAllGems();
}
