﻿/// <summary>
/// Base interface for our IGemType
/// </summary>
public interface IGemType
{
    /// <summary>
    /// Returns the hash code of our type
    /// </summary>
    /// <returns>A hash value that is supposed to be unique to this type</returns>
    int GetHashCode();

    /// <summary>
    /// Returns true if both types are the same
    /// </summary>
    /// <param name="pGemType">IGemType that is going to be compared</param>
    /// <returns>Returns true if both types are the same</returns>
    bool Equals(IGemType pGemType);
}
