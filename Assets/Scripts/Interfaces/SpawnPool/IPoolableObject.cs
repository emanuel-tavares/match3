﻿using UnityEngine;

/// <summary>
/// This interface is used by spawned objects from ISpawned Pools. They are useful to keep a quick reference to the ISpawnPool inside the spawned object.
/// </summary>
public interface IPoolableObject
{
    /// <summary>
    /// Initializes the Spawned Object.
    /// </summary>
    /// <param name="pSpawnPool">Spawn Pool that spawns this object</param>
    /// <returns>True if the IPoolableObject was initialized</returns>
    bool Initialize(ISpawnPool pSpawnPool);

    GameObject Spawn();
    GameObject Spawn(Vector3 vPosition);
    GameObject Spawn(Vector3 vPosition, Transform pParent);
    GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation);
    GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation, Vector3 vScale);
    void Recycle();
}
