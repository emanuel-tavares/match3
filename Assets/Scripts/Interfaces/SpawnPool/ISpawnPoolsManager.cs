﻿using UnityEngine;

/// <summary>
/// This interface is used to group Spawn Pools. 
/// It is useful when you need to spawn different objects in a single script and it's very hard to keep track of which prefab is from which Spawn Pool.
/// </summary>
public interface ISpawnPoolsManager
{
    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <returns>The spawned object</returns>
    GameObject Spawn(GameObject pPrefab);

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <returns>The spawned object</returns>
    GameObject Spawn(GameObject pPrefab, Vector3 vPosition);

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <param name="pParent">Parent of the spawned object</param>
    /// <returns>The spawned object</returns>
    GameObject Spawn(GameObject pPrefab, Vector3 vPosition, Transform pParent);

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <param name="pParent">Parent of the spawned object</param>
    /// <param name="qRotation">Initial rotation of the spawned object</param>
    /// <returns>The spawned object</returns>
    GameObject Spawn(GameObject pPrefab, Vector3 vPosition, Transform pParent, Quaternion qRotation);

    /// <summary>
    /// Spawns a new copy of the prefab from one of our Spawn Pools. If there's no Spawn Pool for said prefab, a new one is created.
    /// </summary>
    /// <param name="pPrefab">Prefab that is used to create spawned copies</param>
    /// <param name="vPosition">Initial position of the spawned object</param>
    /// <param name="pParent">Parent of the spawned object</param>
    /// <param name="qRotation">Initial rotation of the spawned object</param>
    /// /// <param name="vScale">Initial scale of the spawned object.</param>
    /// <returns>The spawned object</returns>
    GameObject Spawn(GameObject pPrefab, Vector3 vPosition, Transform pParent, Quaternion qRotation, Vector3 vScale);

    /// <summary>
    /// Recycles a spawned copy from one of our Spawn Pools. If there's no Spawn Pool for said prefab, the instance is destroyed.
    /// </summary>
    /// <param name="pSpawn">Copy that is going to be recycled</param>
    void Recycle(GameObject pSpawn);
}
