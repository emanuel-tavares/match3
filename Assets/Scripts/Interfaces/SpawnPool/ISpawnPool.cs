﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This interface is used for simple Spawn Pools, data structures that create reusable copies of a GameObject prefab.
/// It primarily spawns another GameObjects, it is quite limited at the moment.
/// </summary>
public interface ISpawnPool
{
    /// <summary>
    /// Creates a pool with a pPrefab with iInitialSize GameObjects instantiated on pool creation
    /// </summary>
    /// <param name="iInitialSize">initial size of pool</param>
    /// <param name="pPrefab">prefab of our pool</param>
    void CreatePool(int iInitialSize = 0, GameObject pPrefab = null);

    /// <summary>
    /// Destroys the pool along with all their instances and the prefab
    /// </summary>
    void DestroyPool();

    /// <summary>
    /// A readable reference to the Prefab used by this ISpawnPool to create another copies
    /// </summary>
    GameObject Prefab { get; }

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <returns>A spawned copy of the prefab</returns>
    GameObject Spawn();

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>    
    GameObject Spawn(Vector3 vPosition);

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <param name="pParent">Parent of our spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>    
    GameObject Spawn(Vector3 vPosition, Transform pParent);

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <param name="pParent">Parent of our spawned copy</param>
    /// <param name="qRotation">Initial rotation of the spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>
    GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation);

    /// <summary>
    /// Enables one of the previously instantiated copies of the prefab. If there's no copies available, a new GameObject is instantiated.
    /// </summary>
    /// <param name="vPosition">Initial position of the spawned copy</param>
    /// <param name="pParent">Parent of our spawned copy</param>
    /// <param name="qRotation">Initial rotation of the spawned copy</param>
    /// <param name="vScale">Initial scale of the spawned copy</param>
    /// <returns>A spawned copy of the prefab</returns>
    GameObject Spawn(Vector3 vPosition, Transform pParent, Quaternion qRotation, Vector3 vScale);

    /// <summary>
    /// Disables one of the previously spawned copies of the prefab.
    /// </summary>
    /// <param name="pSpawn">Copy that is going to be recycled</param>
    void Recycle(GameObject pSpawn);

    /// <summary>
    /// List of prefab copies from our pool, regardless if they are spawned in the scene or not.
    /// </summary>
    IList<GameObject> Instances { get; }
}
