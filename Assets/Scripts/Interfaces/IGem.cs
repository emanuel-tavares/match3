﻿using UnityEngine;

/// <summary>
/// Base interface for the IGems inside every ICell of the IGemGrid.
/// </summary>
public interface IGem
{
    /// <summary>
    /// Returns true if both gems have the same type.
    /// </summary>
    /// <param name="pGem">Gem that is going to be used for comparison</param>
    /// <returns>Returns true if both gems have the same type.</returns>
    bool IsSameType(IGem pGem);

    /// <summary>
    /// Returns the gem type
    /// </summary>
    IGemType Type { get; }

    /// <summary>
    /// Returns the GameObject of this IGem
    /// </summary>
    GameObject GameObject { get; }
}
