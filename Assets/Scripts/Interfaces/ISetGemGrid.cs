﻿/// <summary>
/// This is the 'writable' base interface for our Match 3 grid. With it, you can add and remove gems. Even swap them in places.
/// </summary>
public interface ISetGemGrid : IGemGrid
{
    /// <summary>
    /// Sets a pGem in the following iColumn and iLine on the Grid
    /// </summary>
    /// <param name="iColumn">Column where the new gem will be added</param>
    /// <param name="iLine">Line where the new gem will be added</param>
    /// <param name="pGem">The gem that is going to be added</param>
    /// <returns>True if the new gem was added</returns>
    bool AddGem(int iColumn, int iLine, IGem pGem);

    /// <summary>
    /// Removes the gem from the Cell located in (iColumn, iLine). If bDestroyCurrent is set to true, it destroys the current gem after removal
    /// </summary>
    /// <param name="iColumn">Column from where the new gem will be removed</param>
    /// <param name="iLine">Line from where the new gem will be removed</param>
    /// <param name="bDestroyCurrent">If bDestroyCurrent is set to true, it destroys the current gem after removal</param>
    /// <returns>True if the gem was successfully removed</returns>
    bool RemoveGem(int iColumn, int iLine, bool bDestroyCurrent = true);

    /// <summary>
    /// Swaps the gem values between (iColumn1, iLine1) and (iColumn2, iLine2)
    /// </summary>
    void SwapGems(int iColumn1, int iLine1, int iColumn2, int iLine2);
}
