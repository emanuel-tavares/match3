﻿/// <summary>
/// Base class from the IStates of our IStateMachine
/// </summary>
/// <typeparam name="T"></typeparam>
public interface IState<T>
{
    /// <summary>
    /// Method triggered when the state is activated
    /// </summary>
    /// <param name="pTarget"> Object that uses the State Machine</param>
    /// <param name="pStateMachine"></param>
    void Begin(T pTarget, IStateMachine<T> pStateMachine);

    /// <summary>
    /// Updates the state
    /// </summary>
    /// <param name="fDeltaTime">Delta time since the previous frame</param>
    void UpdateState(float fDeltaTime);

    // Method triggered when the state is deactivated
    void End();
}
