﻿using System.Collections.Generic;

/// <summary>
/// Base interface for our State Machines. This implementation can only have one state active at any time.
/// </summary>
/// <typeparam name="T">Object which uses the state machine</typeparam>
public interface IStateMachine<T>
{
    /// <summary>
    /// Updates the State Machine. It usually means updating the active state.
    /// </summary>
    /// <param name="fDeftaTime">Time passed since the last frame</param>
    void UpdateStateMachine(float fDeftaTime);

    /// <summary>
    /// Adds a new state to our State Machine. Any active state needs to be add beforehand.
    /// </summary>
    /// <param name="pState">The state that is going to be added</param>
    void AddState(IState<T> pState);

    /// <summary>
    /// Removes a new state from our State Machine. 
    /// </summary>
    /// <param name="pState">The state that is going to be removed</param>
    void RemoveState(IState<T> pState);

    /// <summary>
    /// Changes the active state of our State Machine.
    /// </summary>
    /// <param name="pState">The new active state</param>
    void ChangeActiveState(IState<T> pState);

    /// <summary>
    /// List of State that this State Machine has.
    /// </summary>
    IList<IState<T>> States { get; }
}
