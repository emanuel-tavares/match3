﻿using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Base interface for the Cells in IGemGrid.
/// </summary>
public interface ICell
{
    /// <summary>
    /// Column where the ICell is located
    /// </summary>
    int Column { get; }

    /// <summary>
    /// Line where the ICell is located
    /// </summary>
    int Line { get; }

    /// <summary>
    /// World position of our ICell
    /// </summary>
    Vector3 Position { get; }

    /// <summary>
    /// IGem reference from our ICell, in case it has one
    /// </summary>
    IGem Gem { get; }

    /// <summary>
    /// GameObject reference from our ICell
    /// </summary>
    GameObject GameObject { get; }

    /// <summary>
    /// Adds a Drag Listener to the ICell. The 'pCall' method will be called every time a drag gesture occurs over ICell. It needs a trigger collider, by the way.
    /// </summary>
    /// <param name="pCall">UnityAction method that is called when a drag event occurs</param>
    void AddDragListener(UnityAction<ICell, Vector3> pCall);

    /// <summary>
    /// Removes a Drag Listener from the ICell. After that, 'pCall' will not be called after a drag gesture anymore.
    /// </summary>
    /// <param name="pCall">UnityAction method that stops being called when a drag event occurs</param>
    void RemoveDragListener(UnityAction<ICell, Vector3> pCall);

    /// <summary>
    /// Adds a Release Listener to the ICell.  The 'pCall' method will be called every time a release gesture occurs over ICell. It needs a trigger collider, by the way.
    /// </summary>
    /// <param name="pCall">UnityAction method that is called when a release event occurs</param>
    void AddReleaseListener(UnityAction<ICell, Vector3> pCall);

    /// <summary>
    /// Removes a Release Listener to the ICell. After that, 'pCall' will not be called after a release gesture anymore.
    /// </summary>
    /// <param name="pCall">UnityAction method that stops being called when a release event occurs</param>
    void RemoveReleaseListener(UnityAction<ICell, Vector3> pCall);
}
