﻿using System.Collections.Generic;

public class Match
{
    /// <summary>
    /// Cells that take part in the Match
    /// </summary>
    public HashSet<ICell> MatchedCells { get; private set; }

    /// <summary>
    /// Base constructor of Match
    /// </summary>
    /// <param name="iHorizontalMatchSize">Size of the Horizontal Match. Can be zero, however vertical must be higher than that.</param>
    /// <param name="iVerticalMatchSize">Size of the Vertical Match. Can be zero, however horizontal must be higher than that.</param>
    /// <param name="lMatchedCells">All the cells that are part of the match</param>
    public Match(int iHorizontalMatchSize, int iVerticalMatchSize, HashSet<ICell> lMatchedCells)
    {
        MatchedCells = lMatchedCells;
    }

    /// <summary>
    /// Check if the pCell is part of this match
    /// </summary>
    public bool ContainsCell(ICell pCell)
    {
        if (MatchedCells != null)
            return MatchedCells.Contains(pCell);
        return false;
    }
}
