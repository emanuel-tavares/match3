﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Extension methods for arrays and lists
/// </summary>
public static class Extensions
{
    /// <summary>
    /// This method returns a random index from the target array.
    /// </summary>
    /// <typeparam name="T">Array type</typeparam>
    /// <param name="target">Array from where the index comes</param>
    /// <returns>The index</returns>
    public static int GetRandomIndex<T>(this T[] target)
    {
        return Random.Range(0, target.Length);
    }

    /// <summary>
    /// This method returns a random index from the target collection.
    /// </summary>
    /// <typeparam name="T">Collection type</typeparam>
    /// <param name="target">Collection from where the index comes</param>
    /// <returns>The index</returns>
    public static int GetRandomIndex<T>(this ICollection<T> target)
    {
        return Random.Range(0, target.Count);
    }

    /// <summary>
    /// This method returns a random value from the target array.
    /// </summary>
    /// <typeparam name="T">Array type</typeparam>
    /// <param name="target">Array from where the value comes</param>
    /// <returns>The value</returns>
    public static T GetRandomValue<T>(this T[] target)
    {
        int iRndIndex = target.GetRandomIndex();
        return target[iRndIndex];
    }

    /// <summary>
    /// This method returns a random value from the target collection.
    /// </summary>
    /// <typeparam name="T">Collection type</typeparam>
    /// <param name="target">Collection from where the value comes</param>
    /// <returns>The value</returns>
    public static T GetRandomValue<T>(this IList<T> target)
    {
        int iRndIndex = target.GetRandomIndex();
        return target[iRndIndex];
    }
}
