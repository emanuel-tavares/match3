﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class SameTypeTest : MonoBehaviour
{
    [SerializeField]
    private GameObject m_pGem1;
    [SerializeField]
    private GameObject m_pGem2;

    private bool m_bIsValidated;

    protected virtual void OnValidate()
    {
        m_bIsValidated = true;

        // Validating m_pGem1 and m_pGem2. They are only valid if both have an IGem component attached.
        if (m_pGem1 == null || m_pGem1.GetComponent<IGem>() == null)
        {
            Debug.LogError("[Gem]: m_pGemType1 needs an IGem component");
            m_bIsValidated = false;
        }

        if (m_pGem2 == null || m_pGem2.GetComponent<IGem>() == null)
        {
            Debug.LogError("[Gem]: m_pGemType2 needs an IGem component");
            m_bIsValidated = false;
        }
    }

    protected virtual void Update()
    {
        if (m_bIsValidated)
        {
            IGem p1 = m_pGem1.GetComponent<IGem>();
            IGem p2 = m_pGem2.GetComponent<IGem>();
            string sMessage = "[SameTypeTest]: Gems have the same type: " + ((p1.IsSameType(p2)) ? "Yes" : "No");
            Debug.Log(sMessage);
        }
    }

}
